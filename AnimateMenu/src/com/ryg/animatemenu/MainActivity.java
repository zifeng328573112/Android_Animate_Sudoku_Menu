package com.ryg.animatemenu;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.Toast;

import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.Animator.AnimatorListener;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;

public class MainActivity extends Activity implements OnClickListener {

	private static final int MOVE_OUT = 0X01;
	private static final int SPRING_OUT = 0X02;
	private static final int MOVE_BACK = 0X03;
	private static final int SPRING_BACK = 0X04;

	// Rotate动画 - 画面旋转
	private Animation rotateAnimation = null;

	private Button mMenuButton;
	private Button mItemButton1;
	private Button mItemButton2;
	private Button mItemButton3;
	private Button mItemButton4;
	private Button mItemButton5;
	private Button mItemButton6;
	private Button mItemButton7;
	private Button mItemButton8;
	private Button mItemButton9;
	private Button mItemButton10;
	private Button mItemButton11;
	private Button mItemButton12;

	private boolean mIsMenuOpen = false;

	private int animate;
	private int total;
	private Button[] mItemButtons;
	private int[] animatesXs;
	private int[] animatesYs;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		init();
		initView();
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void initView() {
		mMenuButton = (Button) findViewById(R.id.menu);
		mMenuButton.setOnClickListener(this);

		mItemButton1 = (Button) findViewById(R.id.item1);
		mItemButton1.setOnClickListener(this);

		mItemButton2 = (Button) findViewById(R.id.item2);
		mItemButton2.setOnClickListener(this);

		mItemButton3 = (Button) findViewById(R.id.item3);
		mItemButton3.setOnClickListener(this);

		mItemButton4 = (Button) findViewById(R.id.item4);
		mItemButton4.setOnClickListener(this);

		mItemButton5 = (Button) findViewById(R.id.item5);
		mItemButton5.setOnClickListener(this);

		mItemButton6 = (Button) findViewById(R.id.item6);
		mItemButton6.setOnClickListener(this);

		mItemButton7 = (Button) findViewById(R.id.item7);
		mItemButton7.setOnClickListener(this);

		mItemButton8 = (Button) findViewById(R.id.item8);
		mItemButton8.setOnClickListener(this);

		mItemButton9 = (Button) findViewById(R.id.item9);
		mItemButton9.setOnClickListener(this);

		mItemButton10 = (Button) findViewById(R.id.item10);
		mItemButton10.setOnClickListener(this);

		mItemButton11 = (Button) findViewById(R.id.item11);
		mItemButton11.setOnClickListener(this);

		mItemButton12 = (Button) findViewById(R.id.item12);
		mItemButton12.setOnClickListener(this);

		mItemButtons = new Button[] { mItemButton1, mItemButton2, mItemButton3, mItemButton4, mItemButton5, mItemButton6, mItemButton7, mItemButton8, mItemButton9, mItemButton10, mItemButton11,
				mItemButton12 };
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		mMenuButton.performClick();
		getMenuInflater().inflate(R.menu.main, menu);
		return false;
	}

	private void init() {
		// 个数调整（可任意修改1~X个）
		total = 12;

		if (WindowManageUtil.WindowD(MainActivity.this) > 3.0) {
			animate = 500;
			animatesXs = new int[] { 400, 0, -400 };
			animatesYs = new int[] { -300, -500, -600, -900 };
			Log.i("TAG", "超大屏幕手机：" + 500 + " 密度：" + WindowManageUtil.WindowD(MainActivity.this));
		} else if (WindowManageUtil.WindowD(MainActivity.this) > 2.0 && WindowManageUtil.WindowD(MainActivity.this) <= 3.0) {
			animate = 400;
			animatesXs = new int[] { 350, 0, -350 };
			animatesYs = new int[] { -200, -400, -600, -800 };
			Log.i("TAG", "大屏幕手机：" + 400 + " 密度：" + WindowManageUtil.WindowD(MainActivity.this));
		} else if (WindowManageUtil.WindowD(MainActivity.this) > 1.5 && WindowManageUtil.WindowD(MainActivity.this) <= 2.0) {
			animate = 300;
			animatesXs = new int[] { 300, 0, -300 };
			animatesYs = new int[] { -100, -200, -300, -400 };
			Log.i("TAG", "普通屏幕手机：" + 300 + " 密度：" + WindowManageUtil.WindowD(MainActivity.this));
		} else if (WindowManageUtil.WindowD(MainActivity.this) <= 1.5) {
			animate = 200;
			animatesXs = new int[] { 150, 0, -150 };
			animatesYs = new int[] { -80, -160, -240, -320 };
			Log.i("TAG", "小屏幕手机：" + 200 + " 密度：" + WindowManageUtil.WindowD(MainActivity.this));
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.menu:
			if (!mIsMenuOpen) {
				openAnimateButton();
			} else {
				closeAnimateButton();
			}
			break;

		case R.id.item1:
			closeAnimateButton();
			Toast.makeText(this, "你点击了1:" + v.getId(), Toast.LENGTH_SHORT).show();
			break;

		case R.id.item2:
			closeAnimateButton();
			Toast.makeText(this, "你点击了2:" + v.getId(), Toast.LENGTH_SHORT).show();
			break;

		case R.id.item3:
			closeAnimateButton();
			Toast.makeText(this, "你点击了3:" + v.getId(), Toast.LENGTH_SHORT).show();
			break;

		case R.id.item4:
			closeAnimateButton();
			Toast.makeText(this, "你点击了4:" + v.getId(), Toast.LENGTH_SHORT).show();
			break;

		case R.id.item5:
			closeAnimateButton();
			Toast.makeText(this, "你点击了5:" + v.getId(), Toast.LENGTH_SHORT).show();
			break;

		case R.id.item6:
			closeAnimateButton();
			Toast.makeText(this, "你点击了6:" + v.getId(), Toast.LENGTH_SHORT).show();
			break;

		case R.id.item7:
			closeAnimateButton();
			Toast.makeText(this, "你点击了7:" + v.getId(), Toast.LENGTH_SHORT).show();
			break;

		case R.id.item8:
			closeAnimateButton();
			Toast.makeText(this, "你点击了8:" + v.getId(), Toast.LENGTH_SHORT).show();
			break;

		case R.id.item9:
			closeAnimateButton();
			Toast.makeText(this, "你点击了9:" + v.getId(), Toast.LENGTH_SHORT).show();
			break;

		case R.id.item10:
			closeAnimateButton();
			Toast.makeText(this, "你点击了10:" + v.getId(), Toast.LENGTH_SHORT).show();
			break;

		case R.id.item11:
			closeAnimateButton();
			Toast.makeText(this, "你点击了11:" + v.getId(), Toast.LENGTH_SHORT).show();
			break;
			
		case R.id.item12:
			closeAnimateButton();
			Toast.makeText(this, "你点击了12:" + v.getId(), Toast.LENGTH_SHORT).show();
			break;

		default:
			break;
		}
		
	}

	private void openAnimateButton() {
		mIsMenuOpen = true;
		for (int i = 0; i < total; i++) {
			int animateX = 0;
			int animateY = 0;
			if (i == 0 || i == 3 || i == 6 || i == 9) {
				animateX = animatesXs[0];
			} else if (i == 1 || i == 4 || i == 7 || i == 10) {
				animateX = animatesXs[1];
			} else if (i == 2 || i == 5 || i == 8 || i == 11) {
				animateX = animatesXs[2];
			}
			if (i <= 2) {
				animateY = animatesYs[0];
			} else if (i > 2 && i <= 5) {
				animateY = animatesYs[1];
			} else if (i > 5 && i <= 8) {
				animateY = animatesYs[2];
			} else if (i > 8 && i <= 11) {
				animateY = animatesYs[3];
			}
			doAnimateOpen(mItemButtons[i], animateX, animateY, i, total, animate, MOVE_OUT);
		}
	}

	private void closeAnimateButton() {
		mIsMenuOpen = false;
		for (int i = 0; i < total; i++) {
			int animateX = 0;
			int animateY = 0;
			if (i == 0 || i == 3 || i == 6 || i == 9) {
				animateX = animatesXs[0];
			} else if (i == 1 || i == 4 || i == 7 || i == 10) {
				animateX = animatesXs[1];
			} else if (i == 2 || i == 5 || i == 8 || i == 11) {
				animateX = animatesXs[2];
			}
			if (i <= 2) {
				animateY = animatesYs[0];
			} else if (i > 2 && i <= 5) {
				animateY = animatesYs[1];
			} else if (i > 5 && i <= 8) {
				animateY = animatesYs[2];
			} else if (i > 8 && i <= 11) {
				animateY = animatesYs[3];
			}
			doAnimateClose(mItemButtons[i], animateX, animateY, i, total, animate);
		}
	}

	/**
	 * 打开菜单的动画
	 * 
	 * @param view
	 *            执行动画的view
	 * @param index
	 *            view在动画序列中的顺序
	 * @param total
	 *            动画序列的个数
	 * @param radius
	 *            动画半径
	 */
	private void doAnimateOpen(final View view, final int x, final int y, final int index, final int total, final int radius, final int type) {
		int springTranslation = 10;
		if (view.getVisibility() != View.VISIBLE) {
			view.setVisibility(View.VISIBLE);
		}
		final int translationX = x;
		final int translationY = y;
		AnimatorSet set = new AnimatorSet();
		if (MOVE_OUT == type) {
			// 包含平移、缩放和透明度动画
			set.playTogether(ObjectAnimator.ofFloat(view, "rotation", 0, 330),//
					ObjectAnimator.ofFloat(view, "translationX", 0, translationX),//
					ObjectAnimator.ofFloat(view, "translationY", 0, translationY), //
					ObjectAnimator.ofFloat(view, "alpha", 0f, 1f));
			set.setDuration(300).start();
			set.addListener(new AnimatorListener() {
				@Override
				public void onAnimationStart(Animator arg0) {
				}

				@Override
				public void onAnimationRepeat(Animator arg0) {
				}

				@Override
				public void onAnimationEnd(Animator arg0) {
					doAnimateOpen(view, x, y, index, total, radius, SPRING_OUT);
				}

				@Override
				public void onAnimationCancel(Animator arg0) {
				}
			});
		} else if (SPRING_OUT == type) {
			// 包含平移、缩放和透明度动画
			set.playTogether(ObjectAnimator.ofFloat(view, "rotation", 330, 350), ObjectAnimator.ofFloat(view, "translationX", translationX), ObjectAnimator.ofFloat(view, "translationY", translationY));
			set.setDuration(50).start();
			set.addListener(new AnimatorListener() {
				@Override
				public void onAnimationStart(Animator arg0) {
				}

				@Override
				public void onAnimationRepeat(Animator arg0) {
				}

				@Override
				public void onAnimationEnd(Animator arg0) {
					doAnimateOpen(view, x, y, index, total, radius, SPRING_BACK);
				}

				@Override
				public void onAnimationCancel(Animator arg0) {
				}
			});
		} else if (SPRING_BACK == type) {
			// 包含平移、缩放和透明度动画
			set.playTogether(ObjectAnimator.ofFloat(view, "rotation", 350, 360), //
					ObjectAnimator.ofFloat(view, "translationX", translationX, translationX),//
					ObjectAnimator.ofFloat(view, "translationY", translationY, translationY));
			// 动画周期为500ms
			set.setDuration(50).start();
		}
	}

	/**
	 * 关闭菜单的动画
	 * 
	 * @param view
	 *            执行动画的view
	 * @param index
	 *            view在动画序列中的顺序
	 * @param total
	 *            动画序列的个数
	 * @param radius
	 *            动画半径
	 */
	private void doAnimateClose(final View view, int x, int y, int index, int total, int radius) {
		if (view.getVisibility() != View.VISIBLE) {
			view.setVisibility(View.VISIBLE);
		}
		final int translationX = x;
		final int translationY = y;

		AnimatorSet set = new AnimatorSet();
		// 包含平移、缩放和透明度动画
		set.playTogether(ObjectAnimator.ofFloat(view, "rotation", 0, -390), //
				ObjectAnimator.ofFloat(view, "translationX", translationX, translationX),//
				ObjectAnimator.ofFloat(view, "translationY", translationY, translationY));
		// 为动画加上事件监听，当动画结束的时候，我们把当前view隐藏
		set.addListener(new AnimatorListener() {
			@Override
			public void onAnimationStart(Animator animator) {
			}

			@Override
			public void onAnimationRepeat(Animator animator) {
			}

			@Override
			public void onAnimationEnd(Animator animator) {
				AnimatorSet set = new AnimatorSet();
				// 包含平移、缩放和透明度动画
				set.playTogether(ObjectAnimator.ofFloat(view, "rotation", 390, -720),//
						ObjectAnimator.ofFloat(view, "translationX", translationX, 0),//
						ObjectAnimator.ofFloat(view, "translationY", translationY, 0), //
						ObjectAnimator.ofFloat(view, "alpha", 1f, 0f));
				// 为动画加上事件监听，当动画结束的时候，我们把当前view隐藏
				set.addListener(new AnimatorListener() {
					@Override
					public void onAnimationStart(Animator animator) {
					}

					@Override
					public void onAnimationRepeat(Animator animator) {
					}

					@Override
					public void onAnimationEnd(Animator animator) {
						view.setVisibility(View.GONE);
					}

					@Override
					public void onAnimationCancel(Animator animator) {
					}
				});
				set.setDuration(1 * 200).start();
			}

			@Override
			public void onAnimationCancel(Animator animator) {
			}
		});
		set.setDuration(1 * 300).start();
	}

}
