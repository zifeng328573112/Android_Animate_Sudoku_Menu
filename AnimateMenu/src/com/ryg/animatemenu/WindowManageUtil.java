package com.ryg.animatemenu;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Log;

public class WindowManageUtil {

	public static float WindowD(Activity activity) {
		DisplayMetrics metric = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(metric);
		return metric.density;// 屏幕密度（0.75 / 1.0 / 1.5）
	}
	
	public static int WindowW(Activity activity){
		DisplayMetrics metric = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(metric);
		return metric.widthPixels;// 屏幕宽度（像素）
	}
	
	public static int WindowH(Activity activity){
		DisplayMetrics metric = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(metric);
		return metric.heightPixels;// 屏幕高度（像素）
	}
	
	public static int WindowDD(Activity activity){
		DisplayMetrics metric = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(metric);
		return metric.densityDpi;// 屏幕密度DPI（120 / 160 / 240）
	}
	
}
